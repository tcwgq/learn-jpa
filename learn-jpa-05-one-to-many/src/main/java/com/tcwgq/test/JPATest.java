package com.tcwgq.test;

import com.tcwgq.bean.Order;
import com.tcwgq.bean.OrderItem;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPATest {
	private EntityManagerFactory factory = Persistence
			.createEntityManagerFactory("simple");

	@Test
	public void save() {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		Order order = new Order();
		order.setId("123456");
		OrderItem item1 = new OrderItem("吹风机", 49);
		OrderItem item2 = new OrderItem("电风扇", 178);
		OrderItem item3 = new OrderItem("笔记本", 4999);
		order.addOrderItem(item1);
		order.addOrderItem(item2);
		order.addOrderItem(item3);
		em.persist(order);
		em.getTransaction().commit();
		em.close();
		factory.close();
	}
}
