package com.tcwgq.bean;

import javax.persistence.*;

@Entity
@Table(name = "t_person")
public class Person {
	private Integer id;
	private String name;
	private IDCard idcard;

	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length = 50, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "card_id")
	public IDCard getIdcard() {
		return idcard;
	}

	public void setIdcard(IDCard idcard) {
		this.idcard = idcard;
	}

}
