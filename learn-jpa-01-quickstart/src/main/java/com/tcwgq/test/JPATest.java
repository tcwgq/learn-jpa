package com.tcwgq.test;

import com.tcwgq.bean.User;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class JPATest {
	private EntityManagerFactory factory = Persistence
			.createEntityManagerFactory("simple");

	@Test
	public void query() {
		EntityManager em = factory.createEntityManager();
		Query query = em
				.createQuery("select user from User user where user.id = ?1");
		query.setParameter(1, 20);
		// 获取单一结果，结果不存在就会报出异常
		User user = (User) query.getSingleResult();
		System.out.println(user);
		em.close();
		factory.close();
	}

	@Test
	public void delete() {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		Query query = em
				.createQuery("delete from User user where user.id = ?1");
		query.setParameter(1, 20);
		// 获取单一结果，结果不存在就会报出异常
		int result = query.executeUpdate();
		System.out.println(result);
		em.getTransaction().commit();
		em.close();
		factory.close();
	}

	@Test
	public void update() {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		Query query = em.createQuery("update User set name=:name where id=:id");
		query.setParameter("name", "zhanSan");
		query.setParameter("id", 1);
		int result = query.executeUpdate();
		System.out.println(result);
		em.getTransaction().commit();
		em.close();
		factory.close();
	}
}
