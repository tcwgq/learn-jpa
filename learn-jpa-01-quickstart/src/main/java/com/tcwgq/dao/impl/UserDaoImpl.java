package com.tcwgq.dao.impl;

import com.tcwgq.bean.User;
import com.tcwgq.dao.UserDao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class UserDaoImpl implements UserDao {
    private EntityManagerFactory factory = Persistence
            .createEntityManagerFactory("simple");

    @Override
    public void save(User user) {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
        em.close();
        factory.close();
    }

    @Override
    public void update(User user) {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        em.merge(user);
        em.getTransaction().commit();
        em.close();
        factory.close();
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        User user = em.find(User.class, id);
        em.remove(user);
        em.getTransaction().commit();
        em.close();
        factory.close();
    }

    @Override
    public User getUser(Integer id) {
        // 获取数据不需要开启事务
        EntityManager em = factory.createEntityManager();
        User user = em.find(User.class, id);
        // User user = em.getReference(User.class, id);// 相当于load方法，返回的是代理对象
        em.close();
        factory.close();
        return user;

    }

    @Override
    public List<User> getAllUser() {
        // 获取数据不需要开启事务
        EntityManager em = factory.createEntityManager();
        @SuppressWarnings("unchecked")
        List<User> list = em.createQuery("select user from User user")
                .getResultList();
        em.close();
        factory.close();
        return list;
    }

}
