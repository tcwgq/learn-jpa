package com.tcwgq.bean.mapped_super_class;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "pt_emp")
public class PartTimeEmployee extends Employee {
	private Double hourlyWage;

	@Column
	public Double getHourlyWage() {
		return hourlyWage;
	}

	public void setHourlyWage(Double hourlyWage) {
		this.hourlyWage = hourlyWage;
	}

}
