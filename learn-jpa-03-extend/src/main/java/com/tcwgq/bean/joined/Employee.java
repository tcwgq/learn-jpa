package com.tcwgq.bean.joined;

import javax.persistence.*;

@Entity
@Table(name = "emp")
@Inheritance(strategy = InheritanceType.JOINED)
public class Employee {
	private Integer id;
	private String name;

	public Employee() {
		super();
	}

	public Employee(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length = 50, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
