package com.tcwgq.bean.joined;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "pt_emp")
@PrimaryKeyJoinColumn(name = "pt_id")
public class PartTimeEmployee extends Employee {
	private Double hourlyWage;

	@Column
	public Double getHourlyWage() {
		return hourlyWage;
	}

	public void setHourlyWage(Double hourlyWage) {
		this.hourlyWage = hourlyWage;
	}

}
