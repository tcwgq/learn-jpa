package com.tcwgq.bean.general;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Collection;
import java.util.Vector;

@Entity
public class ShoppingCart extends Cart {
	Collection<Item> items = new Vector<>();

	public ShoppingCart() {
		super();
	}

	@OneToMany
	public Collection<Item> getItems() {
		return items;
	}

	public void addItem(Item item) {
		items.add(item);
		incrementOperationCount();
	}
}
