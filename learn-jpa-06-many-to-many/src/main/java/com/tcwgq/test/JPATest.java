package com.tcwgq.test;

import com.tcwgq.bean.Student;
import com.tcwgq.bean.Teacher;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPATest {
	private EntityManagerFactory factory = Persistence
			.createEntityManagerFactory("simple");

	@Test
	public void save() {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		Student student = new Student("张三");
		Teacher teacher = new Teacher("李老师");
		em.persist(student);
		em.persist(teacher);
		em.getTransaction().commit();
		em.close();
		factory.close();
	}

	@Test
	public void buildRelation() {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		Student student = em.find(Student.class, 1);
		Teacher teacher = em.find(Teacher.class, 1);
		// 注意通过关系维护端才能建立关系
		student.addTeacher(teacher);
		em.getTransaction().commit();
		em.close();
		factory.close();
	}

	@Test
	public void removeRelation() {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		Student student = em.find(Student.class, 1);
		Teacher teacher = em.find(Teacher.class, 1);
		// 注意通过关系维护端才能解除关系
		student.removeTeacher(teacher);
		em.getTransaction().commit();
		em.close();
		factory.close();
	}

	@Test
	public void deleteTeacher() {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		// 先通过关系维护端解除关系，然后才能删除老师
		Student student = em.find(Student.class, 1);
		Teacher teacher = em.find(Teacher.class, 1);
		student.removeTeacher(teacher);
		em.remove(teacher);
		em.getTransaction().commit();
		em.close();
		factory.close();
	}

	@Test
	public void deleteStudent() {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		Student student = em.find(Student.class, 1);
		em.remove(student);
		em.getTransaction().commit();
		em.close();
		factory.close();
	}
}
